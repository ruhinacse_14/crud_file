<div class="table-responsive">
	<table class="table table-hover">
	  	<thead>
		    <tr>
		      <th>ID</th>
		      <th>Name</th>
		      <th>Gender</th>
		      <th>Phone</th>
		      <th>Address</th>
		      <th>Action</th>
		    </tr>
	  	</thead>
	    <tbody>
<?php 
include_once('config.php');
$res = $mysqli->query("SELECT * FROM user_info ORDER BY id_no ASC");
while ($row = $res->fetch_assoc()) {?>
		    <tr>
              <td><?php echo $row['id_no'];?></td>
              <td><?php echo $row['name'];?></td>
              <td><?php echo $row['gender'];?></td>
              <td><?php echo $row['phone'];?></td>
              <td><?php echo $row['address'];?></td>
              <td>
              	<a class="btn btn-warning btn-sm" data-toggle="modal" data-target="#myModal<?php echo $row['id_no'];?>">
              		<span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
              	</a>
              	<a onclick="deletedata('<?php echo $row['id_no'];?>')" class="btn btn-danger btn-sm">
              		<span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
              	</a>
              	<div class="modal fade" id="myModal<?php echo $row['id_no'];?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel<?php echo $row['id_no'];?>" aria-hidden="true">
				  <div class="modal-dialog">
				    <div class="modal-content">
				      <div class="modal-header">
				        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				        <h4 class="modal-title" id="myModalLabel<?php echo $row['id_no'];?>">Edit data</h4>
				      </div>
				      <div class="modal-body">
				    	<form>
		                    <div class="form-group">
		                      <label for="nm">Name</label>
		                      <input type="text" class="form-control" id="nm<?php echo $row['id_no'];?>" value="<?php echo $row['name'];?>">
		                    </div>
		                    <div class="form-group">
		                      <label for="gn">Gender</label>
		                      <input type="text" class="form-control" id="gn<?php echo $row['id_no'];?>" value="<?php echo $row['gender'];?>">
		                    </div>
		                    <div class="form-group">
		                      <label for="pn">Phone</label>
		                      <input type="text" class="form-control" id="pn<?php echo $row['id_no'];?>" value="<?php echo $row['phone'];?>">
		                    </div>
		                    <div class="form-group">
		                      <label for="ad">Address</label>
		                      <input type="text" class="form-control" id="ad<?php echo $row['id_no'];?>" value="<?php echo $row['address'];?>">
		                    </div>
		                  </form>
				      </div>
				      <div class="modal-footer">
				        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="updatedata('<?php echo $row['id_no'];?>')">Save changes</button>
				      </div>
				    </div>
				  </div>
				</div>  
              </td>
          </tr>
<?php
    
}
?>
      	</tbody>
    </table>
<?php 
include_once('config.php');
$stmt =  $mysqli->stmt_init();

 $nm = $_POST['nm'];
 $gn = $_POST['gn'];
 $pn = $_POST['pn'];
 $ad = $_POST['ad'];

if($nm != null && $gn != null && $pn != null && $ad != null){
$stmt->prepare("insert into user_info values('',?,?,?,?)");
 $stmt->bind_param("ssss", $nm, $gn, $pn, $ad);


 if($stmt->execute()){ ?>
 <div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Well done!!</strong>You successfully add this.
</div>
 <?php } else { ?>
 <div class="alert alert-danger alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Oh snap!!</strong> Change a few things up and try submitting again. 
</div>
<?php }
 } else{ ?>
<div class="alert alert-warning alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <strong>Warning!</strong> Better check yourself.
</div>
<?php }

?>
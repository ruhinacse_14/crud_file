<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Bootstrap 101 Template</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <style>
    .form_element{
      margin: 60px 0;
    }
    .txt-padding{
      padding: 15px 0;
    }
    </style>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body onload="viewdata()">
  <section class="form_element">
    <div class="container">
      <div class="row txt-padding">
        <div class="col-md-12">
          <!-- Button trigger modal -->
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
            Add New Data
          </button>

          <!-- Modal -->
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Add Data Here</h4>
                </div>
                <div class="modal-body">
                   <form>
                    <div class="form-group">
                      <label for="nm">Name</label>
                      <input type="text" class="form-control" id="nm">
                    </div>
                    <div class="form-group">
                      <label for="gn">Gender</label>
                      <input type="text" class="form-control" id="gn">
                    </div>
                    <div class="form-group">
                      <label for="pn">Phone</label>
                      <input type="text" class="form-control" id="pn">
                    </div>
                    <div class="form-group">
                      <label for="ad">Address</label>
                      <input type="text" class="form-control" id="ad">
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" id="save" class="btn btn-primary">Save changes</button>
                </div>
              </div>
            </div>
          </div>
          <div id="info"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div id="viewdata"></div>
        </div>
      </div>
    </div>
</section>






    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script>
      function viewdata(){
        $.ajax({
          type: "GET",
          url: "php/getdata.php"
            }).done(function(data) {
          $( "#viewdata" ).html(data);
        });
      }
        $('#save').click(function(){

          var nm = $('#nm').val();
          var gn = $('#gn').val();
          var pn = $('#pn').val();
          var ad = $('#ad').val();
          var datas = "nm="+nm+"&gn="+gn+"&pn="+pn+"&ad="+ad;


          $.ajax({
            type: "POST",
            url: "php/newdata.php",
            data: datas,
            dataType: "html"
            }).done(function(data) {
              $( "#info" ).html(data);
                viewdata();
            });
          });
        function updatedata(str){

          var id = str;
          var nm = $('#nm'+str).val();
          var gn = $('#gn'+str).val();
          var pn = $('#pn'+str).val();
          var ad = $('#ad'+str).val();
          var datas = "nm="+nm+"&gn="+gn+"&pn="+pn+"&ad="+ad;


          $.ajax({
            type: "POST",
            url: "php/updatedata.php?id="+id,
            data: datas,
            dataType: "html"
            }).done(function(data) {
              $( "#info" ).html(data);
                viewdata();
            });
          }
        function deletedata(str){

          var id = str;
          $.ajax({
            type: "GET",
            url: "php/deletedata.php?id="+id,
            dataType: "html"
            }).done(function(data) {
              $( "#info" ).html(data);
                viewdata();
            });
          }
    </script>
  </body>
</html>